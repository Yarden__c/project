import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Doctor } from './interfaces/doctor';

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {

  private doctorsApi = 'http://ec2-44-197-171-15.compute-1.amazonaws.com';
  private getdoctors = "get_doctors";
  private setdoctor = "set_doctor";
  private deletedoctor = "del_doctor";
  private uploadimg = "upload_image";
  private recognition = "analyze";
  constructor(private http:HttpClient ) { }

  // public getDoctors() {
  //   return this.http.get<Doctor[]>(`${this.doctorsApi}`);
  // }
  public getDoctors():Observable<Doctor> {
    console.log("in the service")
    return this.http.get<Doctor>(`${this.doctorsApi}/${this.getdoctors}`).pipe(
      map(res =>{
      console.log('in the service 2');
      console.log(res);
      return res;
    }));
    }
    fileUpload(img: FormData):Observable<any> {
      return this.http.post(`${this.doctorsApi}/${this.uploadimg}`,img);
      }

    photoRecognition(image: string):Observable<any>{
      return this.http.get<Doctor>(`${this.doctorsApi}/${this.recognition}/doctorspictures/${image}`);
    }

  public addDoctor(id:string,name:string,department:string,years:number):Observable<Doctor>{
    console.log(id,name,department,years)
    // console.log('jfjfjfjff',(`${this.doctorsApi}/${this.setdoctor}?id=${id}&name=${name}&department=${department}&years=${years}`));
    return this.http.get<Doctor>(`${this.doctorsApi}/${this.setdoctor}?id=${id}&name=${name}&department=${department}&years=${years}`);
  }  

  public deleteDoctor(id:string):Observable<Doctor>{
    console.log(id);
    // console.log('ggg',(`${this.doctorsApi}/${this.deletedoctor}?id=${id}`))
    return this.http.get<Doctor>(`${this.doctorsApi}/${this.deletedoctor}?id=${id}`);
  }

}
