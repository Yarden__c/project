import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { DoctorsService } from '../doctors.service';
import { Doctor } from '../interfaces/doctor';

@Component({
  selector: 'app-doctor-form',
  templateUrl: './doctor-form.component.html',
  styleUrls: ['./doctor-form.component.css']
})
export class DoctorFormComponent implements OnInit {
  editMode:boolean = false;
  id:string;
  name:string;
  department:string;
  years:number;


  onCancel(){
    this.router.navigate(['/doctors']);
  }

  onSubmit(){ 
    if(!this.editMode) {
      console.log('save');
      this.doctorsService.addDoctor(this.id, this.name,this.department,this.years).subscribe(res =>{
        console.log(res)
      });
    }
    else {
      console.log('Edit');
      this.doctorsService.addDoctor(this.id,this.name,this.department,this.years).subscribe(res =>{
        console.log(res)
      });
    }
    this.router.navigate(['/doctors']); 
  }

  addDoctor(){
    this.doctorsService.addDoctor(this.id,this.name,this.department,this.years).subscribe(res =>{
      console.log(res)
    });
    console.log(this.id,this.name,this.department,this.years)
    this.router.navigate(['/doctors']);
  }

  constructor(private router:Router, private doctorsService:DoctorsService) { }

  ngOnInit(): void {
    if(window.history.state?.doctor) {
      let doctor :Doctor = window.history.state.doctor as Doctor;
      this.editMode = true;
      this.id = doctor.id;
      this.name = doctor.name;
      this.department = doctor.department;
      this.years = doctor.years;
  }

}}
