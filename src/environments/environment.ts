// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAbG0XkZktHGHzDIegpHizbjFxQp3e7XuE",
    authDomain: "project-5119d.firebaseapp.com",
    projectId: "project-5119d",
    storageBucket: "project-5119d.appspot.com",
    messagingSenderId: "971127912851",
    appId: "1:971127912851:web:351d297e5e96d42e999066"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
