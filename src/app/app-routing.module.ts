import { RecognitionComponent } from './recognition/recognition.component';
import { PatientsComponent } from './patients/patients.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { DoctorFormComponent } from './doctor-form/doctor-form.component';

const routes: Routes = [
  /* { path: 'customers', component: CustomersComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent }, */
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignUpComponent },
  { path: '', component: HomeComponent },
  { path: 'patientss', component: PatientsComponent },
  { path: 'home', component: HomeComponent },
  { path: 'patients', component: PatientsComponent },
  { path: 'Addpatients', component: PatientFormComponent },
  { path: 'bmi', component: CalculatorComponent },
  { path: 'doctors', component: DoctorsComponent },
  { path: 'Adddoctor', component: DoctorFormComponent },
  { path: 'reco', component: RecognitionComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
