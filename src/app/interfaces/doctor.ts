export interface Doctor {
    id:string;
    name:string;
    department:string;
    years:number;
    similarity?:number;
    imageName?:string,
}
