export interface Patient {
    id?:string,
    name?:string,
    age:number,
    gender?:string,
    height?:number,
    weight?:number,
    sbp?:number,
    dbp?:number,
    cholesterol?:string,
    glucose?:string,
    smoke?:boolean,
    alcohol?:boolean,
    active?:boolean,
    result?:string,

}
