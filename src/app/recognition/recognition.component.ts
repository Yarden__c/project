import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DoctorsService } from '../doctors.service';
import { Doctor } from '../interfaces/doctor';

@Component({
  selector: 'app-recognition',
  templateUrl: './recognition.component.html',
  styleUrls: ['./recognition.component.css']
})
export class RecognitionComponent implements OnInit {
  // image:string ="shiran.jpeg";
  photo:string;
  path:string ="https://doctorspictures.s3.amazonaws.com/";
  match:boolean;
  nomatch:boolean;
  uploudedphoto = false;
  fileObj: File;
  imagename:string;
  element;
  

  
  constructor(private doctorsService:DoctorsService) { }
  

  onFileUpload(event: Event): void{
    console.log(event);
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
    console.log(this.fileObj);
    
    const fileForm = new FormData();
    fileForm.append('img', this.fileObj);
    this.doctorsService.fileUpload(fileForm).subscribe(res => {
      console.log('dddddddddddddd',res);
      console.log('fjfjf',this.imagename)
      this.uploudedphoto = true;
    
      this.photo = res.imageUrl;
      this.doctorsService.photoRecognition(res.filename).subscribe(res =>{
       this.element = res;
      });
      console.log(this.imagename);

      this.match = false;
      this.nomatch = false;

    });
  }
  

  ngOnInit(): void { 
  }

}
