import { Patient } from './../interfaces/patient';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { PatientsService } from './../patients.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'patientForm',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.css']
})
export class PatientFormComponent implements OnInit {
editMode:boolean = false;
userId:string;
name:string;
age:number;
gender:string;
height:number;
weight:number;
sbp:number;
dbp:number;
cholesterol:string;
glucose:string;
id: string;
smoke:boolean = false;
alcohol:boolean = false;
active:boolean = false;
levels: object[] = [{id:1, name:'Normal'},{id:2, name:'Above normal'}, {id:3,name:'Well above normal'}];
level: string;
genders: object[] = [{id:1, name:'Male'},{id:2,name:'Female'}];
rateControl;
onSubmit(){ 
  if(!this.editMode) {
    console.log('save');
    this.patientsServise.addPatient(this.userId,this.name,this.gender,this.age, this.height, this.weight, this.sbp, this.dbp, this.cholesterol, this.glucose, this.smoke, this.alcohol, this.active);
  }
  else {
    console.log('Edit');
    this.patientsServise.updatePatient(this.userId,this.id,this.name,this.gender,this.age, this.height, this.weight, this.sbp, this.dbp, this.cholesterol, this.glucose, this.smoke, this.alcohol, this.active)
  }
  this.router.navigate(['/patients']); 
}

onCancel() {
  this.router.navigate(['/patients']);
}

  constructor(private authService:AuthService ,private patientsServise:PatientsService, private router:Router) { }

  ngOnInit(): void {
    if(window.history.state?.patient) {
      let patiant :Patient = window.history.state.patient as Patient;
      this.editMode = true;
      this.id = patiant.id;
      this.name = patiant.name;
      this.gender = patiant.gender;
      this.age = patiant.age;
      this.alcohol = patiant.alcohol;
      this.cholesterol = patiant.cholesterol;
      this.height = patiant.height;
      this.weight = patiant.weight;
      this.sbp = patiant.sbp;
      this.dbp = patiant.dbp;
      this.smoke = patiant.smoke;
      this.active = patiant.active;
      this.glucose = patiant.glucose;
    }
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
      })
  }

}
