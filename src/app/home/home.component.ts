import { ArticlesService } from './../articles.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Article } from '../interfaces/article';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  articles:Article[] = [];
  article:Article;
  constructor(private articlesService:ArticlesService) { }

  ngOnInit(): void {
    this.articlesService.getArticles().subscribe((res: any) => {
      console.log(res);
      this.articles = res.articles;
    });
    
   }

}
