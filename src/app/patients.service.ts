import { Patient } from './interfaces/patient';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {

userCollection:AngularFirestoreCollection = this.db.collection('users');
patientsCollection:AngularFirestoreCollection;

getPatients(userId): Observable<any[]> {
  this.patientsCollection = this.db.collection(`users/${userId}/patients`, 
     ref => ref.limit(100))
  return this.patientsCollection.snapshotChanges();    
} 

updatePatient(userId:string, id:string, name:string, gender:string, age:number, height:number, weight:number, SBP:number, DBP:number, cholesterol:string, glucose:string, smoke:boolean, alco:boolean, active:boolean,result: string = null){
  this.db.doc(`users/${userId}/patients/${id}`).update(
    {
      name:name,
      gender:gender,
      age:age,
      height:height,
      weight:weight,
      sbp:SBP,
      dbp:DBP,
      cholesterol:cholesterol,
      glucose:glucose,
      smoke:smoke,
      alcohol:alco,
      active:active,
      result:result
    }
  )
}
deletePatient(userId:string, id:string){
  this.db.doc(`users/${userId}/patients/${id}`).delete();
}

addPatient(userId:string, name:string, gender:string, age:number, height:number, weight:number, sbp:number, dbp:number, cholesterol:string, glucose:string, smoke:boolean, alcohol:boolean, active:boolean){
  const patient:Patient = {name:name, gender:gender, age:age, height:height, weight:weight, sbp:sbp, dbp:dbp, cholesterol:cholesterol, glucose:glucose, smoke:smoke, alcohol:alcohol, active:active}
  this.userCollection.doc(userId).collection('patients').add(patient);
} 
  constructor(private db:AngularFirestore) { }
}
