 export interface Article {
     content:string,
     description:string,
     publishedAt:string,
     title:string,
     url:string,
     image:string,
     source: {
         name:string
     }
 }