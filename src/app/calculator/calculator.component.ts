import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  genderpick: string;
  genders: string[] = ['Male', 'Female'];
  heightValue: number = 0;
  weightValue: number = 0;
  result;
  category: string;

  calculateBMI() {
    if (this.heightValue > 0 && this.weightValue > 0) {

      this.result = this.weightValue / ((this.heightValue / 100) * (this.heightValue / 100));
      if (this.result < 18.5) {
        this.category = 'Underweight';
      } else if (this.result >= 18.5 && this.result <= 25) {
        this.category = 'Normal';
      } else if (this.result > 25 && this.result <= 30) {
        this.category = 'Overweight'
      } else if (this.result > 30 && this.result <= 35) {
        this.category = 'Moderately obese'
      } else if (this.result >= 35 && this.result < 40) {
        this.category = 'Severely obese';
      } else {
        this.category = 'Very severely obese';
      }
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

}
