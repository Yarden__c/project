// import { Article } from './interfaces/article';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  // private URL = "http://newsapi.org/v2/top-headlines?country=us&category=health&apiKey=3c4d89028d61452aac705cbf98f53c1f"
  private URL = 'https://gnews.io/api/v4/search?q="heart disease"&token=6da9e992646d2b58399f47f1a08ad088'
  
  
  constructor(private http: HttpClient) { }
  
  getArticles():Observable<any>{
    return this.http.get<any>(this.URL);
    
  }

}
