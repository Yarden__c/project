import { Patient } from './interfaces/patient';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = "https://48w0sv1xfb.execute-api.us-east-1.amazonaws.com/beta";

  predict(patient : Patient ){
    let json = {"data": `${patient.age},${patient.gender},${patient.height},${patient.weight},${patient.sbp},${patient.dbp},${patient.cholesterol},${patient.glucose},${patient.smoke},${patient.alcohol},${patient.active}`};
    console.log(json);
    let body = JSON.stringify(json);
    console.log(body);
    return this.http.post<any>(this.url,body).pipe(map(res => {
      console.log('dsdsdsdss', res);
      return res;
    })); 
  }

  constructor(private http:HttpClient) { }
}
  