import { Router } from '@angular/router';
import { PatientsService } from './../patients.service';
import { Patient } from './../interfaces/patient';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PredictService } from '../predict.service';
import { Sort } from '@angular/material/sort';


@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {
  userId:string;
  patients:Patient[];
  patients$;
  addPatientsFormOpen;
  rowToEdit:number = -1; 
  patientToEdit:Patient = {name:null, gender:null, age:null, height:null, weight:null, sbp:null, dbp:null, cholesterol:null, glucose:null, smoke:null, alcohol:null, active:null};


  
  navigateToCreatePatient(){
    this.router.navigate(['/Addpatients']);
  }


  navigateToEditPatient(index){
    this.router.navigate(['/Addpatients'],{
      state: {
        patient: this.patients[index]
      }
    });  
  }

  deletePatient(index){
    let id = this.patients[index].id;
    this.patientsService.deletePatient(this.userId, id);
  }
  
  displayedColumns: string[] = ['name','age', 'gender', 'height','weight','sbp','dbp','cholesterol','glucose','smoke','alcohol','active' ,'Predict', 'Result', 'Edit','Delete'];
  
  predict(index) {
     this.predictService.predict(this.patients[index]).subscribe(res => {
       this.patients[index].result = res;
       this.updateResult(index);
    });
  }


  
  constructor(private patientsService:PatientsService, public authService:AuthService, private router:Router, private predictService:PredictService) { }

  updateResult(index) {
    const patient = this.patients[index];
    this.patientsService.updatePatient(this.userId,patient.id,patient.name,patient.gender,patient.age, patient.height, patient.weight, patient.sbp, patient.dbp, patient.cholesterol, patient.glucose, patient.smoke, patient.alcohol, patient.active,patient.result);
  }


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      (user) => {
        console.log(user);
          this.userId = user.uid;
          this.patients$ = this.patientsService.getPatients(this.userId);
          this.patients$.subscribe(
            docs => {         
              this.patients = [];
              var i = 0;
              for (let document of docs) {
                const patient:Patient = document.payload.doc.data();
                patient.id = document.payload.doc.id;
                   this.patients.push(patient); 
              }               
            }
          )
      })
  }

}
