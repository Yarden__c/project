import { Doctor } from './../interfaces/doctor';
import { DoctorsService } from './../doctors.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class DoctorsComponent implements OnInit {

  doctors$:Observable<Doctor>;
  id:string;
  name:string;
  department:string;
  years:number;
  doctors:any;

  navigateToCreateDoctor(){
    this.router.navigate(['/Adddoctor']);
  }
  displayedColumns: string[] = ['id','name','department', 'years', 'Edit','Delete'];

  navigateToEditDoctor(index){
    this.router.navigate(['/Adddoctor'],{
      state: {
        doctor: this.doctors[index]
      }
    });  
  }
  
  deleteDoctor(index){
    let id = this.doctors[index].id;
    console.log('ddd',id); 
    this.doctorsService.deleteDoctor(id).subscribe(res =>{
      console.log(res)
      location.reload();});
  }

  constructor(private doctorsService:DoctorsService, private router:Router, public authService:AuthService) { }

  ngOnInit(): void {

    console.log('1');

    this.doctorsService.getDoctors().subscribe(res =>{
      console.log('2');
      console.log('testttttt',res);
      this.doctors = res;
      console.log(this.doctors);
              
    });


    //       this.doctors$.subscribe(
    //         docs => {
    //           this.doctors = [];
    //           var i = 0;
    //           for (let document of docs) {
    //             const doctor:Doctor = document.payload.doc.data();
    //             doctor.id = document.payload.doc.id;
    //                this.doctors.push(doctor); 
    //           }               
    //         }
    //       )
    
  }

}
